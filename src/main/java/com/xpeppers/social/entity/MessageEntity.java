package com.xpeppers.social.entity;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MessageEntity {
	private String message;
	private LocalDateTime creationDate;
	private UserEntity user;
}
