package com.xpeppers.social.entity;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserEntity {
	private String username;
	private List<MessageEntity> personalWall;
	private List<UserEntity> following;
}
