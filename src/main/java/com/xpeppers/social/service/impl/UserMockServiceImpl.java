package com.xpeppers.social.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.xpeppers.social.entity.MessageEntity;
import com.xpeppers.social.entity.UserEntity;
import com.xpeppers.social.exception.UserNotFoundException;
import com.xpeppers.social.service.UserService;

public class UserMockServiceImpl implements UserService {
	
	private Map<String, UserEntity> users;
	
	public UserMockServiceImpl() {
		users = new HashMap<>();
	}

	@Override
	public UserEntity retrieveUserByUsername(String username) throws Exception {
		UserEntity userEntity = users.get(username);
		
		if(Objects.isNull(userEntity)) {
			throw new UserNotFoundException(username);
		}
		
		return userEntity;
	}

	@Override
	public List<MessageEntity> retrieveMessageListByUsername(String username) throws Exception {
		UserEntity user = this.retrieveUserByUsername(username);
		
		return user.getPersonalWall();
	}

	@Override
	public void postMessage(String username, String message) throws Exception {
		UserEntity user;
		
		try {
			user = this.retrieveUserByUsername(username);
		} catch(UserNotFoundException e) {
			user = this.createUserEntity(username);
		}
		
		MessageEntity messageEntity = MessageEntity.builder()
				.creationDate(LocalDateTime.now())
				.user(user)
				.message(message)
				.build();
		
		user.getPersonalWall().add(0, messageEntity);
		
	}

	@Override
	public List<MessageEntity> retrieveWall(String username) throws Exception {
		UserEntity user = this.retrieveUserByUsername(username);

		List<MessageEntity> userFollowingMessages = user.getFollowing().stream()
				.map(item -> item.getPersonalWall())
				.flatMap(Collection::stream)
				.sorted((i1, i2) -> i2.getCreationDate().compareTo(i1.getCreationDate()))
                .collect(Collectors.toList()); 
		
		return userFollowingMessages;
	}
	
	@Override
	public void followUser(String userFollowing, String userFollowed) throws Exception {
		UserEntity userFollowingEntity = this.retrieveUserByUsername(userFollowing);
		UserEntity userFollowedEntity = this.retrieveUserByUsername(userFollowed);
		
		userFollowingEntity.getFollowing().add(userFollowedEntity);
		
	}
	
	private UserEntity createUserEntity(String username) {
		UserEntity user = UserEntity.builder()
				.username(username)
				.personalWall(new ArrayList<>())
				.following(new ArrayList<>())
				.build();
		
		user.getFollowing().add(user);
		
		users.put(username, user);
		
		return user;
	}

}
