package com.xpeppers.social.service;

import java.util.List;

import com.xpeppers.social.entity.MessageEntity;
import com.xpeppers.social.entity.UserEntity;

public interface UserService {

	public UserEntity retrieveUserByUsername(String username) throws Exception;
	
	public List<MessageEntity> retrieveMessageListByUsername(String username) throws Exception;
	
	public void postMessage(String username, String message) throws Exception;
	
	public List<MessageEntity> retrieveWall(String username) throws Exception;
	
	public void followUser(String userFollowing, String userFollowed) throws Exception;
}
