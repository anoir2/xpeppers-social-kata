package com.xpeppers.social.console.instruction.impl;

import com.xpeppers.social.console.instruction.Instruction;

public class FollowInstruction extends BaseInstruction implements Instruction {
	
	@SuppressWarnings("unused")
	private FollowInstruction() {}
	
	private String follower;
	private String followed;
	
	public FollowInstruction(String follower, String followed) throws Exception{
		this.follower = follower;
		this.followed = followed;
	}

	@Override
	public void execute() throws Exception {
		userService.followUser(follower, followed);
	}

}
