package com.xpeppers.social.console.instruction;

import java.util.regex.Matcher;

import com.xpeppers.social.console.instruction.impl.FollowInstruction;
import com.xpeppers.social.console.instruction.impl.PostInstruction;
import com.xpeppers.social.console.instruction.impl.ReadInstruction;
import com.xpeppers.social.console.instruction.impl.WallInstruction;
import com.xpeppers.social.exception.InstructionNotValidException;
import com.xpeppers.social.utils.InstructionConstants;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InstructionHandler {

	public Instruction retrieveInstruction(String instrStr) throws Exception {
		Instruction ins = null;
		Matcher m = null;
		instrStr = instrStr.trim();
		
		if(isReadInstruction(instrStr)) {
			m = InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.READ_CMD_PATTERN, instrStr);
			String username = m.group("username");
			
			ins = new ReadInstruction(username);
		} else if(isPostInstruction(instrStr)) {
			m = InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.POST_CMD_PATTERN, instrStr);
			String username = m.group("username");
			String message = m.group("message");
			
			ins = new PostInstruction(username, message);
		} else if(isFollowInstruction(instrStr)) {
			m = InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.FOLLOW_CMD_PATTERN, instrStr);
			String follower = m.group("follower");
			String followed = m.group("followed");
			
			ins = new FollowInstruction(follower, followed);
		} else if(isWallInstruction(instrStr)) {
			m = InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.WALL_CMD_PATTERN, instrStr);
			String username = m.group("username");
			
			ins = new WallInstruction(username);
		} else {
			throw new InstructionNotValidException();
		}
		
		return ins;
	}
	
	private boolean isPostInstruction(String instruction) {
		return InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.POST_CMD_PATTERN, instruction).matches();
	}
	
	private boolean isReadInstruction(String instruction) {
		return InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.READ_CMD_PATTERN, instruction).matches();
	}
	
	private boolean isFollowInstruction(String instruction) {
		return InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.FOLLOW_CMD_PATTERN, instruction).matches();
	}
	
	private boolean isWallInstruction(String instruction) {
		return InstructionConstants.retrieveMatcherFromRegex(InstructionConstants.WALL_CMD_PATTERN, instruction).matches();
	}
}
