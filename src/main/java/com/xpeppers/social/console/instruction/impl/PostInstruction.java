package com.xpeppers.social.console.instruction.impl;

import com.xpeppers.social.console.instruction.Instruction;

public class PostInstruction extends BaseInstruction implements Instruction {
	
	@SuppressWarnings("unused")
	private PostInstruction() {}
	
	private String username;
	private String message;
	
	public PostInstruction(String username, String message){
		this.username = username;
		this.message = message;
	}

	@Override
	public void execute() throws Exception {
		userService.postMessage(username, message);
	}

}
