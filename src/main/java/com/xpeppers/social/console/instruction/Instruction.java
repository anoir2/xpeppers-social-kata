package com.xpeppers.social.console.instruction;

public interface Instruction {

	public void execute() throws Exception;
}
