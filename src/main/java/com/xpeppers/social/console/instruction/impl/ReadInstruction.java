package com.xpeppers.social.console.instruction.impl;

import java.util.List;

import com.xpeppers.social.console.instruction.Instruction;
import com.xpeppers.social.entity.MessageEntity;
import com.xpeppers.social.utils.DateStringFormatter;

public class ReadInstruction extends BaseInstruction implements Instruction {
	
	@SuppressWarnings("unused")
	private ReadInstruction() {}
	
	private String username;
	
	public ReadInstruction(String username) throws Exception{
		this.username = username;
	}

	@Override
	public void execute() throws Exception {
		List<MessageEntity> messages = userService.retrieveMessageListByUsername(username);
		
		messages.forEach(item -> System.out.println(retrieveWellFormattedMessage(item)));
	}
	
	private String retrieveWellFormattedMessage(MessageEntity message) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("#:>> ")
				.append(message.getMessage())
				.append(" (")
				.append(DateStringFormatter.retrieveFormatterByLocalDate(message.getCreationDate()))
				.append(")");
		
		return builder.toString();
	}

}
