package com.xpeppers.social.console.instruction.impl;

import com.xpeppers.social.config.DIContainer;
import com.xpeppers.social.service.UserService;

public class BaseInstruction {
	
	protected UserService userService;
	
	public BaseInstruction() {
		userService = DIContainer.getInjector().getInstance(UserService.class);
	}

}
