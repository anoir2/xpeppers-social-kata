package com.xpeppers.social.console.instruction.impl;

import java.util.List;

import com.xpeppers.social.console.instruction.Instruction;
import com.xpeppers.social.entity.MessageEntity;
import com.xpeppers.social.utils.DateStringFormatter;

public class WallInstruction extends BaseInstruction implements Instruction {
	
	@SuppressWarnings("unused")
	private WallInstruction() {}
	
	private String username;
	
	public WallInstruction(String username) throws Exception{
		this.username = username;
	}

	@Override
	public void execute() throws Exception {
		List<MessageEntity> messages = userService.retrieveWall(username);
		
		messages.forEach(item -> System.out.println(retrieveWellFormattedMessage(item)));
	}
	
	private String retrieveWellFormattedMessage(MessageEntity message) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("#:>> ")
				.append(message.getUser().getUsername())
				.append(" - ")
				.append(message.getMessage())
				.append(" (")
				.append(DateStringFormatter.retrieveFormatterByLocalDate(message.getCreationDate()))
				.append(")");
		
		return builder.toString();
	}

}
