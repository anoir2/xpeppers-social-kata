package com.xpeppers.social.console;

import java.util.Scanner;

import com.google.inject.Inject;
import com.xpeppers.social.console.instruction.Instruction;
import com.xpeppers.social.console.instruction.InstructionHandler;
import com.xpeppers.social.exception.InstructionNotValidException;
import com.xpeppers.social.exception.UserNotFoundException;

public class ConsoleApp {
	
	private Scanner in;
	
	@Inject
	private InstructionHandler handler;
	
	public ConsoleApp() {
		in = new Scanner(System.in);
	}
	
	public void executeConsole() {
		String input;
		try {
			printMenu();
			while(true) {
				System.out.print("#:> ");
				input = readInput();
				
				handleInstruction(input);
				
				clearConsole();
			}
		} finally {
			System.out.println();
			System.out.println("TERMINATED CONSOLE APP");
			in.close();
		}
		
	}
	
	public void handleInstruction(String instructionStr) {
		Instruction ins = null;
		try {
			
			ins = handler.retrieveInstruction(instructionStr);
			ins.execute();
			
		} catch(InstructionNotValidException e) {
			printNotValidInstruction();
		} catch (UserNotFoundException e) {
			printUserNotFound(e.getUsernameNotFound());
		} catch (Exception e) {
			printGenericError();
		}
	}
	
	private void printMenu() {
		System.out.println("Welcome to Xpeppers Console Social Network!");
		System.out.println();
		System.out.println("The following lines explain the available instructions");
		System.out.println("read syntax: <user name>");
		System.out.println("post syntax: <user name> -> <message>");
		System.out.println("wall syntax: <user name> wall");
		System.out.println("follow syntax: <user name> follows <user name>");
		System.out.println();
	}
	
	private void printNotValidInstruction() {
		System.out.println();
		System.out.print("Instruzione non valida! Premi INVIO per continuare");
		
		in.nextLine();
	}
	
	private void printUserNotFound(String username) {
		System.out.println();
		System.out.println(String.format("%s non e' presente nella lista degli user memorizzati", username));
	}
	
	private void printGenericError() {
		System.out.println();
		System.out.println("C'e' stato un errore.");
	}
	
	private String readInput() {
		
        String s = in.nextLine();
        
        return s;
	}
	
	private void clearConsole() {
		System.out.println();
	}
	
	
}
