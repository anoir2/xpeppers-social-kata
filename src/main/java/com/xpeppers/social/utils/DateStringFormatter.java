package com.xpeppers.social.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public final class DateStringFormatter {
	
	private final static String SECOND_AGO_PATTERN = "%d Second%s ago";
	private final static String MINUTE_AGO_PATTERN = "%d Minute%s ago";
	private final static String DAY_AGO_PATTERN = "%d Day%s ago";
	private final static String DATE_PATTERN = "dd/MM/yyyy HH:mm:ss";
	
	
	private final static Long MINUTE = 60L;
	private final static Long HOUR = MINUTE * 60L;
	private final static Long DAY = HOUR * 24L;
	
	public static final String retrieveFormatterByLocalDate(LocalDateTime date) {
		String message = null;
		String pluralUnitStr = null;
		
		
		if(ChronoUnit.DAYS.between(date, LocalDateTime.now()) > 90) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
	        return date.format(formatter);
		} 
		
		Long secondsDiff = ChronoUnit.SECONDS.between(date, LocalDateTime.now());
		
		if(secondsDiff >= DAY) {
			pluralUnitStr = (secondsDiff - DAY) > DAY ? "s" : "";
			message = String.format(DAY_AGO_PATTERN, (int) Math.floor(secondsDiff / DAY), pluralUnitStr);
		} else if(secondsDiff >= MINUTE) {
			pluralUnitStr = (secondsDiff - MINUTE) > MINUTE ? "s" : "";
			message = String.format(MINUTE_AGO_PATTERN, (int) Math.floor(secondsDiff / MINUTE), pluralUnitStr);
		} else {
			pluralUnitStr = secondsDiff > 1 ? "s" : "";
			message = String.format(SECOND_AGO_PATTERN, (int) Math.floor(secondsDiff), pluralUnitStr);
		}
		
		return message;
	}

}
