package com.xpeppers.social.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstructionConstants {

	public static final String POST_CMD_PATTERN = "^(?<username>[\\w-]+) -> (?<message>[^\\s].*)$";
	public static final String READ_CMD_PATTERN = "^(?<username>[\\w-]+)$";
	public static final String WALL_CMD_PATTERN = "^(?<username>[\\w-]+) wall$";
	public static final String FOLLOW_CMD_PATTERN = "^(?<follower>[\\w-]+) follows (?<followed>[\\w-]+)$";
	
	public static final Matcher retrieveMatcherFromRegex(String regex, String line) {
		 Pattern r = Pattern.compile(regex);

		 Matcher m = r.matcher(line);
		 m.matches();
		 
		 return m;
	}
}
