package com.xpeppers.social.exception;

import lombok.Getter;

@Getter
public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = 5568060383013004819L;
	private String usernameNotFound;
	
	public UserNotFoundException(String username) {
		this.usernameNotFound = username;
	}

}
