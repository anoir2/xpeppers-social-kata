package com.xpeppers.social.exception;

import lombok.Getter;

@Getter
public class InstructionNotValidException extends Exception {

	private static final long serialVersionUID = -1421950470878038395L;

}
