package com.xpeppers.social.config;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.xpeppers.social.console.instruction.InstructionHandler;
import com.xpeppers.social.service.UserService;
import com.xpeppers.social.service.impl.UserMockServiceImpl;

public class BeanConfigModule extends AbstractModule {
	
    @Override
    protected void configure() {
        bind(UserService.class).to(UserMockServiceImpl.class).in(Scopes.SINGLETON);
        bind(InstructionHandler.class).in(Scopes.SINGLETON);
    }
    
}