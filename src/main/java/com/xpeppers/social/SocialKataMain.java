package com.xpeppers.social;

import com.xpeppers.social.config.DIContainer;
import com.xpeppers.social.console.ConsoleApp;

public class SocialKataMain {
	
	public static void main(String args[]) {
		ConsoleApp app = DIContainer.getInjector().getInstance(ConsoleApp.class);
		
		app.executeConsole();
	}

}
