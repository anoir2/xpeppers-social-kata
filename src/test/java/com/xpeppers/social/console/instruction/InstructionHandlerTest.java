package com.xpeppers.social.console.instruction;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.xpeppers.social.config.DIContainer;
import com.xpeppers.social.console.instruction.impl.FollowInstruction;
import com.xpeppers.social.console.instruction.impl.PostInstruction;
import com.xpeppers.social.console.instruction.impl.ReadInstruction;
import com.xpeppers.social.console.instruction.impl.WallInstruction;
import com.xpeppers.social.exception.InstructionNotValidException;
import com.xpeppers.social.service.UserService;

public class InstructionHandlerTest {
	
	private final String USERNAME_ONE_MOCK = "user_one_mock";
	private final String USERNAME_TWO_MOCK = "user_two_mock";
	private final String MESSAGE_ONE_MOCK = "message_one_mock";
	
	private final String READ_INSTRUCTION = USERNAME_ONE_MOCK;
	private final String POST_INSTRUCTION = USERNAME_ONE_MOCK + " -> " + MESSAGE_ONE_MOCK;
	private final String FOLLOW_INSTRUCTION = USERNAME_ONE_MOCK + " follows " + USERNAME_TWO_MOCK;
	private final String WALL_INSTRUCTION = USERNAME_ONE_MOCK + " wall";
	private final String INVALID_POST_INSTRUCTION = USERNAME_ONE_MOCK + "->";

	@Mock
	private UserService userService;
	private InstructionHandler insHand;
	
	private Instruction instruction;
	private String instructionStr;
 
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        
        insHand = DIContainer.getInjector().getInstance(InstructionHandler.class);
    }
    
    @Test
    public void readInstructionTest() throws Exception {
    	givenReadInstruction();
    	whenRetrieveInstruction();
    	thenInstructionIsReadInstruction();
    }
    
    @Test
    public void postInstructionTest() throws Exception {
    	givenPostInstruction();
    	whenRetrieveInstruction();
    	thenInstructionIsPostInstruction();
    }
    
    @Test
    public void followInstructionTest() throws Exception {
    	givenFollowInstruction();
    	whenRetrieveInstruction();
    	thenInstructionIsFollowInstruction();
    }
    
    @Test
    public void wallInstructionTest() throws Exception {
    	givenWallInstruction();
    	whenRetrieveInstruction();
    	thenInstructionIsWalInstruction();
    }
    
    @Test(expected = InstructionNotValidException.class)
    public void notValidInstructionTest() throws Exception {
    	givenInvalidPostInstruction();
    	whenRetrieveInstruction();
    }
    
    /****************************************
     * 										*
     * 				  GIVEN					*
     * 										*
     ****************************************
     */
    
    private void givenReadInstruction() {
    	instructionStr = READ_INSTRUCTION;
    }
    
    private void givenPostInstruction() {
    	instructionStr = POST_INSTRUCTION;
    }
    
    private void givenFollowInstruction() {
    	instructionStr = FOLLOW_INSTRUCTION;
    }
    
    private void givenWallInstruction() {
    	instructionStr = WALL_INSTRUCTION;
    }
    
    private void givenInvalidPostInstruction() {
    	instructionStr = INVALID_POST_INSTRUCTION;
    }
    
    /****************************************
     * 										*
     * 				  WHEN					*
     * 										*
     * @throws Exception 
     ****************************************
     */
    
    private void whenRetrieveInstruction() throws Exception {
    	instruction = insHand.retrieveInstruction(instructionStr);
    }
    
    
    /****************************************
     * 										*
     * 				  THEN					*
     * 										*
     ****************************************
     */
    
    private void thenInstructionIsReadInstruction() {
    	assertTrue(instruction instanceof ReadInstruction);
    }
    
    private void thenInstructionIsPostInstruction() {
    	assertTrue(instruction instanceof PostInstruction);
    }
    
    private void thenInstructionIsFollowInstruction() {
    	assertTrue(instruction instanceof FollowInstruction);
    }
    
    private void thenInstructionIsWalInstruction() {
    	assertTrue(instruction instanceof WallInstruction);
    }
}
