package com.xpeppers.social.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.xpeppers.social.config.DIContainer;
import com.xpeppers.social.entity.MessageEntity;
import com.xpeppers.social.entity.UserEntity;
import com.xpeppers.social.exception.UserNotFoundException;

public class UserServiceTest {
	
	private final String USERNAME_ONE_MOCK = "user_one_mock";
	private final String USERNAME_TWO_MOCK = "user_two_mock";
	private final String USERNAME_NOT_VALID = "user_not_valid";
	private final String MESSAGE_ONE_MOCK = "message_one_mock";
	private final String MESSAGE_TWO_MOCK = "message_two_mock";
	
    private UserService userService;
    private UserEntity userOne;
    private List<MessageEntity> userOneMessageList;
    private List<MessageEntity> userOneWallList;
 
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        
        userService = DIContainer.getInjector().getInstance(UserService.class);
    }
    
    @Test
    public void userPostAMessage() throws Exception {
    	whenUserOnePostMessageOne();
    	whenRetrieveUserOneEntity();
    	whenRetrieveUserOneMessageList();
    	
    	thenUserOneExist();
    	thenUserOneMessageOneIsCorrectlyPosted();
    }
    
    @Test
    public void retrieveValidUser() throws Exception {
    	whenUserOnePostMessageOne();
    	whenRetrieveUserOneEntity();
    	
    	thenUserOneExist();
    }
    
    @Test
    public void retrieveMessageListFromExistingUser() throws Exception {
    	whenUserOnePostMessageOne();
    	whenRetrieveUserOneMessageList();
    	
    	thenUserOneMessageOneIsCorrectlyPosted();
    	
    }
    
    @Test(expected = UserNotFoundException.class)
    public void retrieveNotValidUser() throws Exception {
    	whenUserOnePostMessageOne();
    	whenRetrieveNotValidUser();
    }
    
    @Test
    public void follow_userOneFollowUserTwo() throws Exception {
    	whenUserOnePostMessageOne();
    	whenUserTwoPostMessageOne();
    	whenUserOneFollowUserTwo();
    	whenRetrieveUserOneEntity();
    	
    	thenUserTwoIsInUserOneFollowingList();
    }
    
    @Test(expected = UserNotFoundException.class)
    public void follow_userOneFollowNotValidUser() throws Exception {
    	whenUserOnePostMessageOne();
    	whenUserOneFollowUserNotValid();
    }
    
    @Test(expected = UserNotFoundException.class)
    public void follow_notValidUserFollowUserOne() throws Exception {
    	whenUserOnePostMessageOne();
    	whenUserNotValidFollowUserOne();
    }
    
    @Test
    public void retrieveUserOneWallWithUserTwoMessage() throws Exception {
    	whenUserOnePostMessageOne();
    	whenUserTwoPostMessageOne();
    	whenUserTwoPostMessageTwo();
    	
    	whenUserOneFollowUserTwo();
    	whenRetrieveUserOneWall();
    	
    	thenUserOneWallContainUserTwoMessages();
    }
    
    /****************************************
     * 										*
     * 				  WHEN					*
     * 										*
     ****************************************
     */
    
    public void whenRetrieveUserOneEntity() throws Exception {
    	userOne = userService.retrieveUserByUsername(USERNAME_ONE_MOCK);
    }
    
    public void whenRetrieveUserOneMessageList() throws Exception {
    	userOneMessageList = userService.retrieveMessageListByUsername(USERNAME_ONE_MOCK);
    }
    
    public void whenRetrieveNotValidUser() throws Exception {
    	userOne = userService.retrieveUserByUsername(USERNAME_NOT_VALID);
    }
    
    public void whenUserOnePostMessageOne() throws Exception {
    	userService.postMessage(USERNAME_ONE_MOCK, MESSAGE_ONE_MOCK);
    }
    
    public void whenUserTwoPostMessageOne() throws Exception {
    	userService.postMessage(USERNAME_TWO_MOCK, MESSAGE_ONE_MOCK);
    }
    
    public void whenUserTwoPostMessageTwo() throws Exception {
    	userService.postMessage(USERNAME_TWO_MOCK, MESSAGE_TWO_MOCK);
    }
    
    public void whenUserOneFollowUserTwo() throws Exception {
    	userService.followUser(USERNAME_ONE_MOCK, USERNAME_TWO_MOCK);
    }
    
    public void whenUserTwoFollowUserOne() throws Exception {
    	userService.followUser(USERNAME_TWO_MOCK, USERNAME_ONE_MOCK);
    }
    
    public void whenUserOneFollowUserNotValid() throws Exception {
    	userService.followUser(USERNAME_ONE_MOCK, USERNAME_NOT_VALID);
    }
    
    public void whenUserNotValidFollowUserOne() throws Exception {
    	userService.followUser(USERNAME_NOT_VALID, USERNAME_ONE_MOCK);
    }
    
    public void whenRetrieveUserOneWall() throws Exception {
    	userOneWallList = userService.retrieveWall(USERNAME_ONE_MOCK);
    }
    
    /****************************************
     * 										*
     * 				  THEN					*
     * 										*
     ****************************************
     */
    
    public void thenUserOneExist() throws Exception {
    	String username = userOne.getUsername();
    	assertEquals(username, USERNAME_ONE_MOCK);
    }
    
    public void thenUserOneMessageOneIsCorrectlyPosted() throws Exception {
    	MessageEntity m = userOneMessageList.get(0);
    	assertEquals(m.getMessage(), MESSAGE_ONE_MOCK);
    }
    
    public void thenUserTwoIsInUserOneFollowingList() {
    	List<String> users = userOne.getFollowing().stream()
    			.map(item -> item.getUsername()).collect(Collectors.toList());
    	assertTrue(users.contains(USERNAME_TWO_MOCK));
    }
    
    public void thenUserOneWallContainUserTwoMessages() {
    	long count = userOneWallList.stream()
    			.filter(item -> USERNAME_TWO_MOCK.contentEquals(item.getUser().getUsername()))
    			.map(item -> item.getMessage())
    			.count();
    	
    	assertTrue(count > 0);
    }
    
}
