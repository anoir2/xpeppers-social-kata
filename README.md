# xPeppers - Social media network
## Descrizione funzionale
Il progetto è mirato allo sviluppo di un social network utilizzabile da console. Il social permette la possibilità di interagire con i seguenti 4 comandi:

- **read**: leggere la bacheca personale di un singolo utente;
- **post**: dato un nome utente e un messaggio, scrivere nella bacheca dell'utente specificato 
- **follows**: dati due nomi utenti, seguire i post di un determinato utente e visualizzarli tramite il comando wall
- **wall**: visualizzare il proprio feed con i post di tutti gli utenti seguiti

Per maggiori dettagli, si rimanda alle [specifiche](https://github.com/xpeppers/social_networking_kata).
## Sviluppo
Lo sviluppo è stato effettuato utilizzando gitflow (ho lasciato i branch intatti appositamente) e, per avere un tracing dei task, utilizzando la board di Gitlab. Nelle varie Issue aperte, ogni macro attività ha una sua specifica label utile ad individuare tutte le attività connesse. Per facilità di lettura, ogni commit è agganciato al relativo task poiché è stato adottato lo stile ** ISSUE #<N> - <TEXT> **. Gitlab riesce a riconoscere il numero del commit e lo collega alla issue specificata nel testo.
Per una migliore suddivisione dei task, sarebbe stato ottimale l'utilizzo di Jira (scrittura delle stories e relativo tasks breakdown, link immediato ai subtask ecc..).
Per quanto riguarda i test, sono stati scritti per le due classi principali.

Il tool utilizzato per la gestione delle dipendenze è maven e i framework usati sono i seguenti:
- **Google Guice**: utile per la gestione della DI;
- **Lombok**: utility per la creazione di builder, getter, setter, ecc...
- **JUnit**: Framework per la creazione dei test;
- **Mockito**: Framework per la creazione di oggetti per i test;
## Setup progetto
Per poter importare il progetto correttamente, è necessario che sia installato maven (raccomando la 3.6 in su) e che il proprio IDE sia configurato con lombok ([clicca qui](https://www.baeldung.com/lombok-ide) per maggiori dettagli su come configurarlo).
### Avvio da IDE
Per poter avviare il progetto, selezionare la classe **com.xpeppers.social.SocialKataMain** come classe di bootstrap.
### Generazione pacchetto stand-alone e avvio
Per poter generare il pacchetto .jar in modo corretto, eseguire maven con le phase **clean package** all'interno della cartella di progetto. Il jar sarà posizionato all'interno della cartella **target**.

Per poter avviare l'applicativo utilizzando il pacchetto .jar, eseguire il comando 
>java -jar \<jar path\>
